# Code Quality and Auto Deployment (DevOps)

*Ignore the project source, it's just a template. focus on file `.gitlab.ci.yml`

### Code Quality
> [Code Climate](https://gitlab.com/firdiar/qodecuality/-/snippets/2043021)

> [ESLINT JS (Code Quality)](https://gitlab.com/firdiar/qodecuality/-/snippets/2043022)


### Auto Deployment
> [Auto Deployment Heroku](https://gitlab.com/firdiar/qodecuality/-/snippets/2043025)


### Result
> [See the result example here](https://gitlab.com/firdiar/qodecuality/-/pipelines)