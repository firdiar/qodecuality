'use strict';

const express = require('express');
const bodyParser = require('body-parser');
let send = require('./lib/sendMsg')

const server = express();
server.use(bodyParser.urlencoded({
    extended: true
}));

server.use(bodyParser.json());

//variabel default
let nama
let pesan = ''
let room_id
let payload

server.post('/',(req,res)=>{
    // Bot terima input dari user berupa room_id, pesan, dan nama user
    room_id = res.req.body.chat_room.qiscus_room_id
    let msg = res.req.body.message.text
    nama = res.req.body.from.fullname

    //konversi pesan yang mengandung huruf kapital menjadi huruf kecil
    let lower = msg.toLowerCase()

    if(lower === '/halo'){
        pesan = 'halo ' + nama + ' ! Aku asisten pribadi mu, ada yang bisa dibantu?'
        payload = {
            'text': pesan,
            'buttons': [{
                    'label': 'Tombol Reply Text',
                    'type': 'postback',
                    'payload': {
                        'url': '#',
                        'method': 'get',
                        'payload': 'null'
                    }
                },
                {
                    'label': 'Tombol Link',
                    'type': 'link',
                    'payload': {
                        'url': 'https://www.google.com',
                    }
                }
            ]
        }
        send.Btn(room_id,payload)
    }
    if(lower === '/menu'){
        pesan = "Aku Bisa melakukan ini"
        payload = {
            'cards' : [
                {
                    'image' : 'https://cdns.img.com/a.jpg',
                    'title' : 'Gambar 1',
                    'description' : 'Carousel Double Button',
                    'default_action' : {
                        'type' : 'postback',
                        'postback_text' : 'Load More...',
                        'payload' : {
                            'url' : 'https://j.id',
                            'method' : 'GET',
                            'payload': null
                        }
                    },
                    'buttons' : [
                        {
                            'label' : 'Button 1',
                            'type' : 'postback',
                            'postback_text' : 'Load More...',
                            'payload' : {
                                'url' : 'https://www.r.com',
                                'method' : 'GET',
                                'payload' : null
                            }
                        },
                        {
                            'label' : 'Button 2',
                            'type' : 'postback',
                            'postback_text' : 'Load More...',
                            'payload' : {
                                'url' : 'https://www.r.com',
                                'method' : 'GET',
                                'payload' : null
                            }
                        }
                    ]
                },
                {
                    'image' : 'https://res.cloudinary.com/hgk8.jpg',
                    'title' : 'Gambar 2',
                    'description' : 'Carousel single button',
                    'default_action' : {
                        'type' : 'postback',
                        'postback_text' : 'Load More...',
                        'payload' : {
                            'url' : 'https://j.id',
                            'method' : 'GET',
                            'payload': null
                        }
                    },
                    'buttons' : [
                        {
                            'label' : 'Button 1',
                            'type' : 'postback',
                            'postback_text' : 'Load More...',
                            'payload' : {
                                'url' : 'https://www.r.com',
                                'method' : 'GET',
                                'payload' : null
                            }
                        }
                    ]
                }
            ]
        }
        send.Txt(pesan,room_id)
        send.Carousel(room_id,payload) 
    }
    if(lower === '/card'){
        payload = {
            'text' : 'Special deal buat sista nih..',
            'image' : 'https://cdns.img.com/a.jpg',
            'title' : 'Gambar 1',
            'description' : 'Card Double Button',
            'url' : 'http://url.com/baju?id=123%26track_from_chat_room=123',
            'buttons' : [
                {
                    'label' : 'Button 1',
                    'type' : 'postback',
                    'postback_text' : 'Load More...',
                    'payload' : {
                        'url' : 'https://www.r.com',
                        'method' : 'GET',
                        'payload' : null
                    }
                },
                {
                    'label' : 'Button 2',
                    'type' : 'postback',
                    'postback_text' : 'Load More...',
                    'payload' : {
                        'url' : 'https://www.r.com',
                        'method' : 'GET',
                        'payload' : null
                    }
                }
            ]
        }
        send.Card(room_id,payload)
    }
})

server.listen((process.env.PORT || 3000), () => {
    console.log("Server is up and running..."+`${process.env.PORT}`);
});